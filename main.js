$(document).ready(function () {
  var typed = new Typed('.typed', {
    stringsElement: '#typed-strings',
    typeSpeed: 80,
    showCursor: true,
    cursorChar: '_',
    backDelay: 2500,
    backSpeed: 8,
    loop: true,
    loopCount: Infinity
  });
});